const $ = require('jquery');
const hideButtonTemplate = require('./hide-button.ejs');
const lscache = require('ls-cache');
const STORAGE_KEY = 'prpp-hidden-';
// Storage values will persist for 7 days.
const STORAGE_EXPIRE_TIME = 10080;
const AUTO_HIDE_EXTENSIONS = ['npm-shrinkwrap.json', '.svg', '.png', '.jpg', '.eslintrc', '.xml'];

export default class BitBucketHideButtons {
  constructor() {

    const observer = new MutationObserver(this._listenForDiffFileMutations.bind(this));

    const config = {
      childList: true,
      subtree: true,
      characterData: true
    };

    this.$buttons = null;

    observer.observe($('body').get(0), config);
  }

  _listenForDiffFileMutations(mutations) {
    const that = this;

    mutations.forEach(function (mutation) {
      const findAndModifyAddedNodeOfInterest = (addedNodes) => {
        const isAddedNodeOfInterest = function ($addedNode) {
          return $addedNode.hasClass('diff-actions') || $addedNode.find('.diff-actions').length > 0;
        };

        addedNodes.forEach((addedNode) => {
          const $addedNode = $(addedNode);

          if (isAddedNodeOfInterest($addedNode)) {
            that._addToggleBtnsToDOM($addedNode, that);
            $('.MJ-toggleFileBtn').click(that._handleToggleFileBtnClick);
          }
        });
      };

      const addedNodes = Array.prototype.slice.call(mutation.addedNodes);

      if (!Array.isArray(addedNodes)) {
        return;
      }

      findAndModifyAddedNodeOfInterest(addedNodes);
    });
  }

  _addToggleBtnsToDOM($addedNode, that) {
    const $diffActionContainers = $addedNode.hasClass('diff-actions') ?
      $addedNode : $addedNode.find('.diff-actions');

    $diffActionContainers.prepend(hideButtonTemplate());

    $diffActionContainers.parents('.commentable-diff').each(function () {
      const commentableDiff = $(this);
      const fileName = commentableDiff.attr('data-filename');
      const itemFromStorage = lscache.get(STORAGE_KEY + fileName);

      if (itemFromStorage === true) {
        commentableDiff.find('.diff-content-container').toggle();
        that._hideFile(commentableDiff.find('.MJ-toggleFileBtn'), commentableDiff.find('.heading'));
      } else if (itemFromStorage === null) {
        AUTO_HIDE_EXTENSIONS.forEach(function (extension) {
          if (fileName.indexOf(extension) > -1) {
            commentableDiff.find('.diff-content-container').toggle();
            that._hideFile(commentableDiff.find('.MJ-toggleFileBtn'), commentableDiff.find('.heading'));
          }
        });
      }
    });
    this.$buttons = $diffActionContainers.find('.MJ-toggleFileBtn');
  }

  _handleToggleFileBtnClick() {
    const $toggleFileBtn = $(this);
    const $diffContent = $toggleFileBtn.parents('.diff-container').find('.diff-content-container');
    const $diffFileHeading = $toggleFileBtn.parents('.heading');

    $diffContent.toggle();
    const fileName = $toggleFileBtn.parents('.commentable-diff').attr('data-filename');

    if (/Hide/.test($toggleFileBtn.text())) {
      $toggleFileBtn.text('Show File');
      $diffFileHeading.css({
        'border-bottom': '1px solid #ccc',
        'border-radius': '5px'
      });
      lscache.set(STORAGE_KEY + fileName, true, STORAGE_EXPIRE_TIME);
    } else {
      $toggleFileBtn.text('Hide File');
      $diffFileHeading.removeAttr('style');
      lscache.set(STORAGE_KEY + fileName, false, STORAGE_EXPIRE_TIME);
    }
  }

  _hideFile($toggleFileBtn, $diffFileHeading) {
    $toggleFileBtn.text('Show File');
    $diffFileHeading.css({
      'border-bottom': '1px solid #ccc',
      'border-radius': '5px'
    });
  }
}

