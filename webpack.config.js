/*global __dirname */

const webpack = require('webpack');
const path = require('path');

module.exports = {
  entry: {
    content: './app/content-script/content-script.js',
    popup: './app/popup/popup.js'
  },

  output: {
    path: path.join(__dirname, 'dist'),
    filename: '[name].bundle.js',
    publicPath: '/dist/'
  },

  module: {
    loaders: [
      {
        test: /\.jsx?$/,
        exclude: /(node_modules|bower_components)/,
        loader: 'babel',
        query: {
          presets: ['es2015', 'stage-0']
        }
      },
      {
        test: /\.ejs$/,
        loader: "ejs-loader?variable=data"
      },
      {
        test: /\.less$/,
        loader: "style-loader!css-loader!less-loader"
      },
      {
        test: /\.css$/,
        loader: "style-loader!css-loader"
      },
      {
        test: /\.jpe?g$|\.gif$|\.png$|\.svg$|\.woff$|\.ttf$|\.wav$|\.mp3$/,
        loader: "file-loader"
      }
    ]
  },

  devtool: 'source-map',

  plugins: [
    //new webpack.HotModuleReplacementPlugin(), // live reload page without losing state of app
    //new webpack.optimize.DedupePlugin(), // search for equal/similar files and deduplicate them out
    //new webpack.NoErrorsPlugin() // avoid emitting files when an error occurs
  ]
};